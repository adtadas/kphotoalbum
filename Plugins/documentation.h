//krazy:skip
/**
  \namespace Plugins
  \brief Implementation of the Purpose interface.

  ## KIPI Status:
  The old KIPI interface is no longer supported by KPhotoAlbum.

  ## Purpose Status:
   - Sharing images is possible
   - Success/error feedback does not yet work correctly
**/
// vi:expandtab:tabstop=4 shiftwidth=4:
